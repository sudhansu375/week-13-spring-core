package com.glearning.spring.core;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
	
	public static void main(String[] args) {
		//client - dependency management is done by the client
		/*
		 * UberGoDriver hari = new UberGoDriver();
		 * 
		 * Passenger ravi = new Passenger(hari);
		 */
		//spring will scan the file and start creating the object.
		// After the object is created, the bean will be registered inside the application context
		// We can request the bean from the application context with the bean name.
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
		
		Passenger ravi1 = applicationContext.getBean("passenger", Passenger.class);
		Passenger ravi2 = applicationContext.getBean("passenger", Passenger.class);
		
		System.out.println("Checking if Ravi1 and Ravi2 are pointing to the same memory address :: "+ (ravi1 == ravi2));
		//ravi.commute("BTM-2nd Cross", "BIAL Airport", 18);
		
		/*
		 * String[] strArray = applicationContext.getBeanDefinitionNames(); System.out.
		 * println("===================== Bean Name starts ==============================="
		 * ); for(String bean: strArray) { System.out.println(bean); } System.out.
		 * println("===================== Bean Name ends ==============================="
		 * );
		 */
		//close the application context
		AbstractApplicationContext abstractApplicationContext = (AbstractApplicationContext) applicationContext;
		abstractApplicationContext.registerShutdownHook();
	}

}
